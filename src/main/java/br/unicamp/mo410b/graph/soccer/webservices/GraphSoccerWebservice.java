package br.unicamp.mo410b.graph.soccer.webservices;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.unicamp.mo410b.graph.soccer.AreaResponse;
import br.unicamp.mo410b.graph.soccer.NodeResponse;
import br.unicamp.mo410b.graph.soccer.db.GraphSoccerService;

/**
 * Webservice respons�vel por retornar os resultados das consultas feitas pela
 * classe GraphSoccerService.
 *
 */
@RestController
public class GraphSoccerWebservice {

	@Autowired
	private GraphSoccerService soccerService;

	@RequestMapping(value = "/pathGols", method = RequestMethod.GET)
	public List<NodeResponse> getPathOfGols(@RequestParam(value = "ActionId") Long id) {

		return soccerService.getGolNodePaths(id);
	}
	
	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public List<NodeResponse> getEvents(@RequestParam(value = "action", defaultValue = "14") Integer action,
										@RequestParam(value = "time", defaultValue = "1") Integer time) {
		
		return soccerService.getEvents(action, time);
	}


	@RequestMapping(value = "/avgArea", method = RequestMethod.GET)
	public List<AreaResponse> getAverageArea(@RequestParam(value = "player1") Integer player1,
			@RequestParam(value = "player2") Integer player2, @RequestParam(value = "player3") Integer player3,
			@RequestParam(value = "initialFrame", defaultValue = "0") Integer initialFrame, 
			@RequestParam(value = "finalFrame", defaultValue = "90000") Integer finalFrame,
			@RequestParam(value = "time", defaultValue = "1") Integer time)  {

		return soccerService.getAverageArea(player1, player2, player3, initialFrame, finalFrame, time);
	}
	
	@RequestMapping(value = "/players", method = RequestMethod.GET)
	public Set<NodeResponse> getPlayers(@RequestParam(value = "ActionId") Long id,			
			@RequestParam(value = "sameTeam", defaultValue = "BOTH") Team sameTeam,			
			@RequestParam(value = "distance", defaultValue = "-1") Double distance) {
	
		return soccerService.getPlayers(id, sameTeam, distance);
	}
	
	@RequestMapping(value = "/player/allFrames", method = RequestMethod.GET)
	public List<NodeResponse> getPlayers(@RequestParam(value = "player") Long player,
			@RequestParam(value = "initialFrame", defaultValue = "0") Integer initialFrame, 
			@RequestParam(value = "finalFrame", defaultValue = "90000") Integer finalFrame,
			@RequestParam(value = "time", defaultValue = "1") Integer time)  {
	
		return soccerService.getPlayerInAllFrames(player, initialFrame, finalFrame, time);
	}
	
	@RequestMapping(value = "/lastFrame", method = RequestMethod.GET)
	public Long getLastFrame(@RequestParam(value = "time", defaultValue = "1") Integer time)  {
	
		return soccerService.getLastFrame(time);
	}
	
	@RequestMapping(value = "/connect", method = RequestMethod.GET)
	public Boolean connect()  {
		return Boolean.TRUE;
	}
}