package br.unicamp.mo410b.graph.soccer.util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.TransactionTerminatedException;
import org.neo4j.rest.graphdb.RestAPI;
import org.neo4j.rest.graphdb.RestAPIFacade;
import org.neo4j.rest.graphdb.query.QueryEngine;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import br.unicamp.mo410b.graph.soccer.db.RelTypes;

/**
 * Essa classe é apenas utilitária que usei para popular o banco de eventos.
 * Inclusive comentei o método main pois ele causa conflito quando subimos o
 * Webservice
 * 
 * @author vntmasc
 *
 */
public class RestEventFileReader {

//	public static void main(String[] args) throws IOException {
//		
//		
//		RestAPI graphDb = new RestAPIFacade("http://104.196.24.193:80/db/data/","neo4j", "eliezer123");
//
//		createGraphEventsAndConectWithPlayers("arquivosAlunosFutebol/capspot1scout.ant", graphDb, 1);
//		createGraphEventsAndConectWithPlayers("arquivosAlunosFutebol/capspot2scout.ant", graphDb, 2);
//	}

	private static void createGraphEventsAndConectWithPlayers(String filePath, RestAPI graphDb, 
			Integer time) throws IOException {

		Charset charset = Charset.forName("UTF-8");
		List<String> lines = Files.readAllLines(Paths.get(filePath), charset);
		Node lastNode = null;
		for (String string : lines) {
			Node node = null;
			try (Transaction tx = graphDb.beginTx()) {

				String[] values = string.split(" ");
				Map<String,Object> props=new HashMap<String, Object>();  

				Long frame = Long.parseLong(values[0]);
				props.put("frame", frame);

				Integer player = Integer.parseInt(values[1]);
				props.put("player", player);

				Double x = Double.parseDouble(values[2]);
				props.put("x", x);

				Double y = Double.parseDouble(values[3]);
				props.put("y", y);

				Integer action = Integer.parseInt(values[4]);
				props.put("action", action);
				
				props.put("time", time);

				node = graphDb.createNode(props);

				Label labelNodeType = DynamicLabel.label("SOCCER_EVENT");
				node.addLabel(labelNodeType);				
				
				if (values[5] != null) {
					Boolean actionExecution = null;
					if (values[5].trim().equals("1"))
						actionExecution = Boolean.TRUE;
					else if (values[5].trim().equals("0"))
						actionExecution = Boolean.FALSE;
					node.setProperty("actionExecution", actionExecution);
				}

				if (lastNode != null) {
					lastNode.createRelationshipTo(node, RelTypes.NEXT);
					node.createRelationshipTo(lastNode, RelTypes.PREVIOUS);
				}
				Node nodePosition = getPlayer(player, frame, graphDb, time);
				if (nodePosition != null) {
					node.createRelationshipTo(nodePosition, RelTypes.EVENT);
					nodePosition.createRelationshipTo(node, RelTypes.EVENT_POS);
				}
				tx.success();
			} catch (TransactionTerminatedException e) {
				throw new RuntimeException(e);
			}
			lastNode = node;
		}

	}

	public static Node getPlayer(Integer player, Long frame, RestAPI graphDb, Integer time) {
		
		QueryEngine engine= new RestCypherQueryEngine(graphDb);  
		String query = "MATCH (n) WHERE n.IdJogador=" + player + " and n.frame=" + frame +" and n.tempo="+time+ " RETURN n ";
		Node node = null;
		QueryResult<Map<String,Object>> result= engine.query(query, Collections.EMPTY_MAP);
		Iterator<Map<String, Object>> iterator=result.iterator();  
		if (iterator.hasNext()) {
			Map<String, Object> row = iterator.next();
			node = (Node) row.get("n");
		}
		return node;
	}
}
