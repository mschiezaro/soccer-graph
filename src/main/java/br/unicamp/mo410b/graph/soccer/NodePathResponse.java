package br.unicamp.mo410b.graph.soccer;

import java.util.List;

public class NodePathResponse {
	
	private List<NodeResponse> nodes;

	public List<NodeResponse> getNodes() {
		return nodes;
	}

	public void setNodes(List<NodeResponse> nodes) {
		this.nodes = nodes;
	}
	
	

}
