package br.unicamp.mo410b.graph.soccer.db;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;

public class EventEvaluator implements Evaluator {
	
	// Se houve desarmes (5 e 6) ou falta(13) antes do gol
	private Set<Integer> importatEvents =  new HashSet<Integer>(Arrays.asList(5,6, 13));
	
	// Eventos que param a busca e deixam inconclusiva os motivos dos gols:
	// - Desarme do adversario (Se teve um desarme do adversario, em instantes praximo ao gol deveriamos ter 
	//   analisado eventos mais importantes, ou seja, nao seguiu uma sequencia)
	// - Tiro-de-meta
	// - Lateral 
	// - Escanteio
	// - Tiro Livre
	private Set<Integer> breakEvents =  new HashSet<Integer>(Arrays.asList(9,10, 11, 12));

	@Override
	public Evaluation evaluate(final Path path) {

		if (path.length() == 0) {
			return Evaluation.EXCLUDE_AND_CONTINUE;
		}
		
		Node playerAction = path.startNode();
		Node endNode = path.endNode();
		
		Integer action =(Integer)endNode.getProperty("action");
		Boolean actionExecution =(Boolean)endNode.getProperty("actionExecution");
		
		if (playerAction.equals(endNode)) {
			return Evaluation.INCLUDE_AND_CONTINUE;
		}
		if(breakEvents.contains(action)) {
			return Evaluation.INCLUDE_AND_PRUNE;
		}
		if (actionExecution && importatEvents.contains(action)) {
			return Evaluation.INCLUDE_AND_PRUNE;
		}
		return Evaluation.INCLUDE_AND_CONTINUE;
	}

	private boolean differentTeam(Node nodeAction, Node nodeUnderAnalysis) {
		Integer playerActionNumber = (Integer) nodeAction.getProperty("player");
		Integer playerUnderAnalysisNumber = (Integer) nodeUnderAnalysis.getProperty("player");
		boolean resPlayerAction1 = between(playerActionNumber, 1, 14);
		boolean resPlayerAction2 = between(playerActionNumber, 15, 28);
		boolean resPlayeUnderAnalysis1 = between(playerUnderAnalysisNumber, 1, 14);
		boolean resPlayeUnderAnalysis2 = between(playerUnderAnalysisNumber, 15, 28);

		if (!(resPlayerAction1 ^ resPlayerAction2) || !(resPlayeUnderAnalysis1 ^ resPlayeUnderAnalysis2)) {
			throw new RuntimeException("Jogadores n�o podem estar no mesmo time ao mesmo tempo");
		}
		return ((resPlayerAction1 ^ resPlayeUnderAnalysis1) && (resPlayerAction2 ^ resPlayeUnderAnalysis1)) ;
	}

	boolean between(int value, int min, int max) {
		return (value >= min) && (value <= max);
	}
}
