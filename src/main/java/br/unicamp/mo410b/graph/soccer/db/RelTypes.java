package br.unicamp.mo410b.graph.soccer.db;

import org.neo4j.graphdb.RelationshipType;

public enum RelTypes implements RelationshipType {
	NEXT, PREVIOUS, EVENT, EVENT_POS
}