package br.unicamp.mo410b.graph.soccer.db;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.TransactionTerminatedException;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.unicamp.mo410b.graph.soccer.NodeResponse;
import br.unicamp.mo410b.graph.soccer.AverageAreaResponse;
import br.unicamp.mo410b.graph.soccer.AreaResponse;
import br.unicamp.mo410b.graph.soccer.webservices.Team;


/**
 * Classe responsavel para realizar as consultas no banco de grafos
 *
 */
@Component
public class GraphSoccerService {

	// private static final String DB_PATH =
	// "C:/projects/mo410b-server/db/default.graphdb";

	@Value("${dbPath}")
	private String dbPath;

	private GraphDatabaseService db;

	public GraphSoccerService() {
		System.out.println("Instantiation");
	}

	@PostConstruct
	public void setDb() {
		File file = new File(dbPath);
		db = new GraphDatabaseFactory().newEmbeddedDatabase(dbPath);
	}

	
	public Set<NodeResponse> getPlayers(Long id, Team sameTeam, Double distance) {

		String query = "match (n:SOCCER_EVENT)-[r1:EVENT]-(p:Posicao)-[r2:distancia]-(op:Posicao)" + 
						" where ID(n) = "+id;

		if (distance != null && distance != -1) {
			query += " and r2.valor <=" + distance;

		}

		if ( ! Team.BOTH.equals(sameTeam) ) {
			
			if(Team.SAME.equals(sameTeam)) {
				
				query += " and ((p.IdJogador <= 14 and op.IdJogador <= 14) or (p.IdJogador > 14 and op.IdJogador > 14))";
				
			} else if (Team.DIFFERENT.equals(sameTeam)) {
				query += " and ((p.IdJogador <= 14 and op.IdJogador > 14) or (p.IdJogador > 14 and op.IdJogador <= 14))";
			}
		}

		query += " return p, op";
		Set<NodeResponse> response = new HashSet<NodeResponse>();
		try (Transaction ignored = db.beginTx();
				
			Result result = db.execute(query)) {	
				while (result.hasNext()) {
					Map<String, Object> row = result.next();
					for (Entry<String, Object> column : row.entrySet()) {
						Node node = (Node)column.getValue();
						NodeResponse nodeResponse = new NodeResponse();
						Integer player = ((Long) node.getProperty("IdJogador")).intValue();
						nodeResponse.setPlayer(player);
						Integer frame = ((Long) node.getProperty("frame")).intValue();
						nodeResponse.setFrame(frame);
						nodeResponse.setX((Double) node.getProperty("x"));
						nodeResponse.setY((Double) node.getProperty("y"));
						Integer tempo = ((Long) node.getProperty("tempo")).intValue();
						nodeResponse.setTime(tempo);
						nodeResponse.setId(node.getId());
						response.add(nodeResponse);				
					}
				}
				ignored.success();
			} catch (TransactionTerminatedException e) {
				throw new RuntimeException(e);
			}
			return response;
	}	
	
	/**
	 * Pega todos os nos de Eventos passada por parametro (action) e a partir
	 * dele pega os nos anteriores (frames anteriores). A ideia eh montar um
	 * cenario do que oriou o evento originou por parametro. Foram definidos os
	 * seguintes eventos como quebra de sequencia para finalizar a lista:
	 * Tiro-de-meta Lateral Escanteio Tiro Livre Desarmes e faltas executadas
	 * com sucesso
	 * 
	 * @param aid Id auto gerado do No
	 *            1-  Primeiro tempo ou 1 - segundo tempo (pois o frame eh por tempo)
	 * @return Lista de NodeResponse lista de nos com o cenario que originou o evento. Os nos estao em ordem decrescente,
	 *         ou seja o evento mais recente o primeiro da lista. Quem define quantos objetos tem na lista e o que deve 
	 *         ser incluido na classe EventEvaluator.
	 */
	public List<NodeResponse> getGolNodePaths(Long id) {
		
		try (Transaction ignored = db.beginTx()) {

			Node nodeAction = db.getNodeById(id);
			
			// Objetos de resposta (Path) para o no
			Path lastPosition = null;
			for (Path position : db.traversalDescription().depthFirst()
					.relationships(RelTypes.PREVIOUS, Direction.OUTGOING).evaluator(new EventEvaluator())
					.traverse(nodeAction)) {

				lastPosition = position;
			}
			// lista com o Path (nos encontrados)
			Iterable<Node> nodes = lastPosition.nodes();
			// monto a lista de Objetos de resposta para que no webservice
			// seja criado o json
			List<NodeResponse> paths = new ArrayList<NodeResponse>();
			for (Node node : nodes) {
				NodeResponse nodeRes = new NodeResponse();
				nodeRes.setFrame((Integer) node.getProperty("frame"));
				nodeRes.setPlayer((Integer) node.getProperty("player"));
				nodeRes.setAction((Integer) node.getProperty("action"));
				nodeRes.setActionExecution((Boolean) node.getProperty("actionExecution"));
				nodeRes.setX((Double) node.getProperty("x"));
				nodeRes.setY((Double) node.getProperty("y"));
				nodeRes.setTime((Integer) node.getProperty("time"));					
				nodeRes.setId(node.getId());
				paths.add(nodeRes);
			}
			ignored.success();
			return paths;
		} catch (TransactionTerminatedException e) {
			throw new RuntimeException(e);
		}
	}

	public List<AreaResponse> getAverageArea(Integer player1, Integer player2, Integer player3, Integer initialFrame, 
			Integer finalFrame, Integer time) {

		double accumulatedArea = 0;

		List<AreaResponse> areaResponses = new ArrayList<AreaResponse>();
		
		String query = "match (a)-[r]-(b), (a)-[r1]-(c), (b)-[r2]-(c) " + "where " + "a.IdJogador = " + player1
				+ " and " + "b.IdJogador = " + player2 + " and " + "c.IdJogador = " + player3
				+ " and a.frame >= "+initialFrame+" and a.frame <= "+finalFrame+" and a.tempo = "+time
				+ " return r.valor, r1.valor, r2.valor, a.frame";
		int count = 0;
		AverageAreaResponse ret = new AverageAreaResponse();
		try (Transaction ignored = db.beginTx();

				Result result = db.execute(query)) {
			
			while (result.hasNext()) {
				count++;
				Map<String, Object> row = result.next();
				List<Double> values = new ArrayList<Double>();
				values.add((Double)row.get("r.valor"));
				values.add((Double)row.get( "r1.valor"));
				values.add((Double)row.get("r2.valor"));
				Long frame = (Long)row.get("a.frame");

				double area = calculateArea(values);
				areaResponses.add(new AreaResponse(area, frame,time));
				accumulatedArea += area;
			}
			ignored.success();
			ret.setAreaResponses(areaResponses);
		} catch (TransactionTerminatedException e) {
			throw new RuntimeException(e);
		}
		ret.setAverageArea(calculateAvgArea(accumulatedArea, count));
		return areaResponses;
	}
	
	public List<NodeResponse> getEvents(Integer action, Integer time) {
		
		String query = "match (e:SOCCER_EVENT) where e.action = "+action+" and e.time = "+time+" return e";
		
		List<NodeResponse> response = new ArrayList<NodeResponse>();
		try (Transaction ignored = db.beginTx();

				Result result = db.execute(query)) {
			while (result.hasNext()) {
				Map<String, Object> row = result.next();
				Node node = (Node)row.get("e");
				NodeResponse nodeResponse = new NodeResponse();
				nodeResponse.setFrame((Integer) node.getProperty("frame"));
				nodeResponse.setPlayer((Integer) node.getProperty("player"));
				nodeResponse.setAction((Integer) node.getProperty("action"));
				nodeResponse.setActionExecution((Boolean) node.getProperty("actionExecution"));
				nodeResponse.setX((Double) node.getProperty("x"));
				nodeResponse.setY((Double) node.getProperty("y"));
				nodeResponse.setTime((Integer) node.getProperty("time"));					
				nodeResponse.setId(node.getId());
				response.add(nodeResponse);				
			}
			ignored.success();
		} catch (TransactionTerminatedException e) {
			throw new RuntimeException(e);
		}
		return response;
	}

	public List<NodeResponse> getPlayerInAllFrames(Long player, Integer 
			
			initialFrame, Integer finalFrame, Integer time) {

		String query = "match (p:Posicao) where p.IdJogador = "+player+" and p.frame >= "+
		initialFrame+" and p.frame <= "+finalFrame+" and p.tempo = "+time+
		" optional match (p)-[r:EVENT_POS]-(e:SOCCER_EVENT) return p, e.action as a, e.actionExecution as e";

		List<NodeResponse> response = new ArrayList<NodeResponse>();
		try (Transaction ignored = db.beginTx();

				Result result = db.execute(query)) {
			while (result.hasNext()) {
				Map<String, Object> row = result.next();
				Node node = (Node)row.get("p");
				Integer action = (Integer)row.get("a");
				Boolean actionExecution = (Boolean)row.get("e");
				NodeResponse nodeResponse = new NodeResponse();
				nodeResponse.setPlayer(player.intValue());
				Integer frame = ((Long) node.getProperty("frame")).intValue();
				nodeResponse.setFrame(frame);
				nodeResponse.setX((Double) node.getProperty("x"));
				nodeResponse.setY((Double) node.getProperty("y")); 
				nodeResponse.setTime(time);
				nodeResponse.setId(node.getId());
				if( action == null || actionExecution == null) {
					nodeResponse.setAction(-1);
					nodeResponse.setActionExecution(false);
				} else {
					nodeResponse.setAction(action);
					nodeResponse.setActionExecution(actionExecution);
				}
				response.add(nodeResponse);				
			}
			ignored.success();
		} catch (TransactionTerminatedException e) {
			throw new RuntimeException(e);
		}
		return response;
	}
	
	public Long getLastFrame(Integer time) {
		
		String query = "match (p:Posicao) where p.tempo = "+time+" return MAX(p.frame) as f";

		Long maxFrame = 0l;
		try (Transaction ignored = db.beginTx();

				Result result = db.execute(query)) {
			while (result.hasNext()) {
				Map<String, Object> row = result.next();
				maxFrame = (Long)row.get("f");
			}
			ignored.success();
		} catch (TransactionTerminatedException e) {
			throw new RuntimeException(e);
		}
		return maxFrame;
	}
		

	private double calculateArea(List<Double> distances) {

		double d1 =  distances.get(0);
		double d2 =  distances.get(1);
		double d3 =  distances.get(2);
		double aux = (d1 + d2 + d3) / 2;
		double aux2 = aux * (aux - d1) * (aux - d2) * (aux - d3);
		double area = Math.sqrt(aux2);
		return area;
	}

	private double calculateAvgArea(double accumulatedArea, Integer limitFrames) {

		return accumulatedArea / limitFrames;
	}
}
