package br.unicamp.mo410b.graph.soccer;

import java.io.Serializable;
import java.util.List;

public class AverageAreaResponse implements Serializable {

	private static final long serialVersionUID = 2954839249620853621L;

	private List<AreaResponse> areaResponses;

	private Double averageArea;		

	public AverageAreaResponse() {

	}

	public AverageAreaResponse(List<AreaResponse> averageAreaResponse, Double averageArea) {
		this.areaResponses = averageAreaResponse;
		this.averageArea = averageArea;
	}
	
	public Double getAverageArea() {
		return averageArea;
	}

	public List<AreaResponse> getAreaResponses() {
		return areaResponses;
	}

	public void setAreaResponses(List<AreaResponse> areaResponses) {
		this.areaResponses = areaResponses;
	}

	public void setAverageArea(Double averageArea) {
		this.averageArea = averageArea;
	}
}
