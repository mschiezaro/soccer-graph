package br.unicamp.mo410b.graph.soccer;

import java.io.Serializable;

public class AreaResponse implements Serializable {


	private static final long serialVersionUID = -1068202131081839687L;

	private Double area;

	private Long frame;
	
	private Integer time;

	public AreaResponse() {

	}

	public AreaResponse(Double area, Long frame, Integer time) {
		this.area = area;
		this.frame = frame;
		this.time = time;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public Long getFrame() {
		return frame;
	}

	public void setFrame(Long frame) {
		this.frame = frame;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}
}
