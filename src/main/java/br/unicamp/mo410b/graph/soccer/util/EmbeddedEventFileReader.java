package br.unicamp.mo410b.graph.soccer.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.TransactionTerminatedException;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import br.unicamp.mo410b.graph.soccer.db.RelTypes;

/**
 * Essa classe é apenas utilitária que usei para popular o banco de eventos.
 * Inclusive comentei o método main pois ele causa conflito quando subimos o
 * Webservice
 * 
 * @author vntmasc
 *
 */
public class EmbeddedEventFileReader {

//	public static void main(String[] args) throws IOException {
//		
//		File file = new File("C:/Users/vntmasc/Documents/Neo4j/default.graphdb");
//
//		GraphDatabaseService graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(file);
//
//		createGraphEventsAndConectWithPlayers("arquivosAlunosFutebol/capspot1scout.ant", graphDb, 1);
//		createGraphEventsAndConectWithPlayers("arquivosAlunosFutebol/capspot2scout.ant", graphDb, 2);
//		EmbeddedEventFileReader.registerShutdownHook(graphDb);
//	}

	private static void createGraphEventsAndConectWithPlayers(String filePath, GraphDatabaseService graphDb,
			Integer time) throws IOException {

		Charset charset = Charset.forName("UTF-8");
		List<String> lines = Files.readAllLines(Paths.get(filePath), charset);
		Node lastNode = null;
		for (String string : lines) {
			Node node = null;
			try (Transaction tx = graphDb.beginTx()) {

				node = graphDb.createNode();

				String[] values = string.split(" ");

				Label labelNodeType = DynamicLabel.label("SOCCER_EVENT");
				node.addLabel(labelNodeType);

				Long frame = Long.parseLong(values[0]);
				node.setProperty("frame", frame);

				Integer player = Integer.parseInt(values[1]);
				node.setProperty("player", player);

				Double x = Double.parseDouble(values[2]);
				node.setProperty("x", x);

				Double y = Double.parseDouble(values[3]);
				node.setProperty("y", y);

				Integer action = Integer.parseInt(values[4]);
				node.setProperty("action", action);
				
				node.setProperty("time", time);

				if (values[5] != null) {
					Boolean actionExecution = null;
					if (values[5].trim().equals("1"))
						actionExecution = Boolean.TRUE;
					else if (values[5].trim().equals("0"))
						actionExecution = Boolean.FALSE;
					node.setProperty("actionExecution", actionExecution);
				}

				if (lastNode != null) {
					lastNode.createRelationshipTo(node, RelTypes.NEXT);
					node.createRelationshipTo(lastNode, RelTypes.PREVIOUS);
				}
				Node nodePosition = getPlayer(player, frame, graphDb, time);
				if (nodePosition != null) {
					node.createRelationshipTo(nodePosition, RelTypes.EVENT);
					nodePosition.createRelationshipTo(node, RelTypes.EVENT_POS);
				}
				tx.success();
			} catch (TransactionTerminatedException e) {
				throw new RuntimeException(e);
			}
			lastNode = node;
		}

	}
	
	public static Node getPlayer(Integer player, Long frame, GraphDatabaseService graphDb, Integer time) {
		String query = "MATCH (n) WHERE n.IdJogador=" + player + " and n.frame=" + frame +" and n.tempo="+time+ " RETURN n ";
		Node node = null;
		Result result = graphDb.execute(query);
		if (result.hasNext()) {
			Map<String, Object> row = result.next();
			node = (Node) row.get("n");
		}
		return node;
	}

	private static void registerShutdownHook(final GraphDatabaseService graphDb) {
		// Registers a shutdown hook for the Neo4j instance so that it
		// shuts down nicely when the VM exits (even if you "Ctrl-C" the
		// running application).
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				graphDb.shutdown();
			}
		});
	}

}
